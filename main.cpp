#include<iostream>
#include<vector>
#include<string>
#include <eigen3/Eigen/Dense>
#include<fstream>
#include "cmdline.h"

using namespace std;
using namespace Eigen;

string lidar_path, lidar_path_out, distance_out;
int left_crop, right_crop;
Matrix3f M_proj;
Matrix4f M_init;

string project_matrix_file, initial_matrix_file;

void ParseArguments(int argc, char* argv[])
{
    cmdline::parser commandParser;

    commandParser.add<int>("left_crop", 'l', "left crop value", false, 150);
    commandParser.add<int>("right_crop", 's', "right crop value", false, 50);
    commandParser.add<string>("project_matrix", 'p', "project_matrix_file", false, "");
    commandParser.add<string>("initial_matrix",'i',"intial matrix file that decides which point to score", false, "");

    commandParser.add("help", 'h', "display this message");
    commandParser.set_program_name("ConvertLidar");
    commandParser.footer("lidar_file, lidar_file_out, distance_out");
    bool isCorrectCommandline = commandParser.parse(argc, argv);
	// Check arguments

    if (!isCorrectCommandline) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommandline || commandParser.rest().size() < 3) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	lidar_path = commandParser.rest()[0];
	lidar_path_out = commandParser.rest()[1];
	distance_out = commandParser.rest()[2];

    project_matrix_file=commandParser.get<string>("project_matrix");
    initial_matrix_file=commandParser.get<string>("initial_matrix");
}

inline Matrix3f ReadProjMatrix(const char* filename)
{
    Matrix3f M_Proj;
    if(!strcmp(filename,""))
    {
        M_Proj(0, 0) = 999.7838133341337;
        M_Proj(0, 1) = 0.0;
        M_Proj(0, 2) = 792.9412307739258;
        M_Proj(1, 0) = 0.0;
        M_Proj(1, 1) = 999.7838133341337;
        M_Proj(1, 2) = 609.6846313476562;
        M_Proj(2, 0) = 0.0;
        M_Proj(2, 1) = 0.0;
        M_Proj(2, 2) = 1.0;
    }
    else
    {
        ifstream v_c(filename);
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                v_c>>M_Proj(i,j);
            }
        }
    }
    return M_Proj;
}

inline Matrix4f ReadInitialMatrix(const char* M_init_name)
{
    Matrix4f M_init;
    if(!strcmp(M_init_name,""))
    {
        M_init(0,0)=-0.00809358404391232;
        M_init(0,1)=-0.999967572308700;
        M_init(0,2)=-0.00237435960422023;
        M_init(0,3)=0.281037177488261;
        M_init(1,0)=-0.0105632042466814;
        M_init(1,1)=0.00246090913771785;
        M_init(1,2)=-0.999945693119595;
        M_init(1,3)=-0.0888997514524071;
        M_init(2,0)=0.999913274796053;
        M_init(2,1)=-0.00806227990468220;
        M_init(2,2)=-0.0105744138392157;
        M_init(2,3)=-0.694856368943206;
        M_init(3,0)=0;
        M_init(3,1)=0;
        M_init(3,2)=0;
        M_init(3,3)=1;
    }
    else
    {
        std::ifstream myfile(M_init_name);
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                myfile>>M_init(i,j);
            }
        }
        myfile.close();
    }

    return M_init;
}


std::vector<VectorXf> parseVelodynefile(const char *Velodynefile)
{
    std::vector<VectorXf> laser_points;
    std::ifstream input(Velodynefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << Velodynefile << std::endl;
        return laser_points;
    }

    float tmp;
    while (!input.eof()) {
        VectorXf point(6);
        input >> tmp;
        point(0) = tmp / 100.0;
        input >> tmp;
        point(1) = tmp / 100.0;
        input >> tmp;
        point(2) = tmp / 100.0;
        input >> tmp;
        point(3) = tmp;
        input >> tmp;
        point(4) = tmp;
        input >> tmp;
        point(5) = tmp;
        laser_points.push_back(point);
    }
    return laser_points;
}

vector<Vector3f> ProcessPoints(vector<VectorXf> laser_points, vector<double>& lengths)
{
    int pt_num=laser_points.size();
    vector<Vector3f> newLaserPoints;
    lengths.clear();
    const int m=1600, n=848;
    for(int i=0;i<pt_num;i++)
    {
        VectorXf point=laser_points[i];
        //cout<<point<<endl;
        if(point(0)==0 && point(1)==0 && point(2)==0)
            continue;

        VectorXf X(4);
        X<<point(0),point(1),point(2),1;
        //cout<<X<<endl;
        VectorXf X2=M_init*X;
        Vector3f X_trans2;
        X_trans2<<X2(0),X2(1),X2(2);
        Vector3f UV2=M_proj*X_trans2;
        float u2=UV2(0)/UV2(2), v2=UV2(1)/UV2(2);

        if(u2-1<=left_crop || u2>=m-right_crop || v2-1<=0 || v2>=n-1 || X2(2)<=0)
            continue;

        Vector3f np(X(0),X(1),X(2));
        double length=sqrt(np(0)*np(0)+np(1)*np(1)+np(2)*np(2));
        np=np/length;
        newLaserPoints.push_back(np);
        lengths.push_back(length);
    }
    return newLaserPoints;
}

void Save(vector<Vector3f> newLaserPoints, vector<double> lengths)
{
    ofstream myfile(lidar_path_out.c_str()), myfile2(distance_out.c_str());
    int pt_num=newLaserPoints.size();
    for(int i=0;i<pt_num;i++)
    {
        myfile<<newLaserPoints[i](0)<<" "<<newLaserPoints[i](1)<<" "<<newLaserPoints[i](2)<<endl;
        myfile2<<lengths[i]<<endl;
    }
    myfile.close();
}

int main(int argc, char *argv[])
{
    ParseArguments(argc, argv);
    M_proj=ReadProjMatrix(project_matrix_file.c_str());
    M_init=ReadInitialMatrix(initial_matrix_file.c_str());
    vector<VectorXf> laser_points = parseVelodynefile(lidar_path.c_str());
    vector<double> lengths;
    vector<Vector3f> newLaserPoints = ProcessPoints(laser_points, lengths);
    Save(newLaserPoints, lengths);
    return 0;
}
