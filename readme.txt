This project converts the lidar points to training and testing files.

Input: should be points without outliers.

Output: two files: one is the direction and the other is the distance.